# Widescreen for Grand Theft Auto San Andreas
This Project is oriented towards Wine (Linux, Mac) users (but probably works under Windows as well).

| 1366 x 768 Screenshot |
| --------------------- |
| <img src="Screenshot.png"> |

## Why?
A lot of mods or widescreen fixes leading to a broken game under Wine and results into errors during the launch of GTA San Andreas

## Requirements
The install of the original game via wine in Version 1.00. If using the Steamversion or Version 1.01. search for a downgrader of GTA San Andreas.

## What does it come with?
- Widescreen settings
- Slightly enhanced Graphics
- Performance improvements
- Better gamepad support

This all is mostly done via mods.

## Install
1. Install the original game via wine, if not already done (perhaps add d3dx9 via winetricks, to be shure everything works fine)
2. Download the WidescreenWine folder
3. Extract the modloader.7z The structure oft the WidescreenWine directory should then look like this:
    - models (folder)
    - modloader (folder)
    - scripts (folder)
    - modloader.asi
    - vorbisFile.asi
    - vorbisHooked.dll
4. Paste all the content of WidscreenWine to the folder of your GTA SA install (e.g. C:Program Files (x86)\Rockstar Games\GTA San Andreas\)
5. Launch the game as usually

## Big Thanks To the Author(s)
If you made this, pleas tell me and you can get at least be mentioned here.
